const NodeRSA = require('node-rsa');

const key = new NodeRSA({b : 512});
key.setOptions({encryptionScheme: 'pkcs1'});

const PUBLIC_KEY = key.exportKey('pkcs8-public-pem');
const PRIVATE_KEY = key.exportKey('pkcs1-pem');
let Encrypted = ""




 ///Encryption

    const encrypt = (data) =>{
            key.importKey(PUBLIC_KEY, 'pkcs8-public-pem');
            const encrypted = key.encrypt(JSON.stringify(data), 'base64');
            console.log('ENCRYPTED:');
            console.log(encrypted);
            Encrypted = encrypted;

            return encrypted

    }
 ///


 ///Decryption
    const decrypt = () => {
            key.importKey(PRIVATE_KEY, 'pkcs1-pem');

            // read the encrypted data from service call
            const encrypted = Encrypted;
            const decryptedString = key.decrypt(encrypted, 'utf8');

            console.log('\nDECRYPTED string: ');
            console.log(decryptedString);

            const decrypedObject = JSON.parse(decryptedString);

            return decrypedObject
    }
 ///

 module.exports = { encrypt,decrypt }
