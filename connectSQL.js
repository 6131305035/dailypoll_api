const express = require('express')
const bodyParser = require('body-parser')
const app = express()

const router = express.Router();

require('dotenv').config()
const encode = require("./encode");
const dbConfig = require("./db");
const { json } = require('body-parser');

let cors = require('cors');
let cron = require('node-cron')


const mockdata = [{
    "Loc_ID": 1,
    "Loc_lat": 116.3426552,
    "Loc_long": -8.7149312
  }, {
    "Loc_ID": 2,
    "Loc_lat": 123.444424,
    "Loc_long": 10.0620494
  }, {
    "Loc_ID": 3,
    "Loc_lat": -70.1338535,
    "Loc_long": -17.3753589
  }, {
    "Loc_ID": 4,
    "Loc_lat": 36.6012942,
    "Loc_long": 35.3740182
  }, {
    "Loc_ID": 5,
    "Loc_lat": 90.204816,
    "Loc_long": 28.599774
  }]

const prefix = process.env.PREFIX
const port = process.env.PORT_API

app.use(cors())
app.use(express.json())
//app.use(bodyParser.json)
//app.use(bodyParser.urlencoded({ extended : true}))

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})


//-------------------------------------------------------------//

let user = {
    id : '',
    username : '',
    password : '',
    role : 'admin'
}


app.get(prefix + '/me', (req, res) =>{
    return res.json({
        data : {
            user
        }
    })
})


app.post(prefix + '/login', (req, res) => {
    const { am_username,am_password } = req.body
    let sql = "SELECT am_username ,am_password, am_role from Admin_mng am "
    dbConfig.beginTransaction(function(err) {
        if(err) { throw err; }
        let query = dbConfig.query(sql, (err,results) => {
            if (err) { 
                dbConfig.rollback(function() {
                    console.log(err) 
                    throw err;
                })
                
            }
            let id = ''
            let role = ''
            let username_found = results.some(el => el.am_username = `${am_username}` )
            let password_found = results.some(el => el.am_password = `${am_password}` )

            if(username_found && password_found ){

               for(let i = 0 ; i < results.length;i++){
                if(am_username == results[i].am_username && am_password == results[i].am_password){
                    id = results[i].am_id
                    role = results[i].am_role
                }
               }

               if(role == 'admin'){
                        user.id = id;
                        user.username = am_username
                        user.password = am_password
                        user.role = role
                        return res.json({
                            data: {
                                user ,
                            token: 'THIS_IS_TOKEN'
                            }
                        });
               }else if(role == 'manager'){
                        user.id = (results.length + 1);
                        user.username = am_username;
                        user.password = am_password;
                        user.role = role
                        return res.json({
                            data: {
                                user ,
                            token: 'THIS_IS_TOKEN'
                            }
                        });
               }
            } else {
              return res.status(401).json({
                message : 'Invalid Username or Password'
              });
            }
            
        })
    })

} )
/////////////////////////////////////////////////////////////////
///////////               PollDetail             ///////////////
/////////////////////////////////////////////////////////////////

//Get all
app.get(prefix + '/getPollDetail_Table',(req, res) => {
    let sql = "select * from `PollDetail`"
    dbConfig.beginTransaction(function(err) {
        if(err) { throw err; }
        let query = dbConfig.query(sql, (err,results) => {
            if (err) { 
                dbConfig.rollback(function() {
                    console.log(err) 
                    throw err;
                })
                
            }
            console.log(results)
    
            return res.send(results);
            
        })
    })
    
})

//Get By ID
app.get(prefix + "/getPollDetail_Table/:id",(req , res) => {
  let sql = "select * from `PollDetail` where Que_ID = " + req.params.id
  let query = dbConfig.query(sql, (err,results) => {
      if (err) { console.log(err) }
      
      console.log(results[0])

      return res.send(results[0]);
  });
  
});

//Create User
app.post(prefix + '/postPollDetail_Table/', function(req,res) {
        
  //req.body = {id : 6 , email : 'email006', name : 'name006' , active : false };
  let data = req.body;
  let en_data = encode.encrypt(data);
  if (!encode.decrypt()) {
      return res.status(400).send({ success : false, message : 'Please Provide body' })
  }
  let json_encrypted = encode.decrypt()

  json_encrypted.User_ID = parseInt(json_encrypted.User_ID)
  json_encrypted.Loc_lat = parseFloat(json_encrypted.Loc_lat)
  json_encrypted.Loc_long = parseFloat(json_encrypted.Loc_long)

  dbConfig.query("insert into `PollDetail` set ?", json_encrypted , function (error, results , fields) {
      console.log(error) ;
      console.log("save appt ---->");
      console.log(results)
      return res.send({ success: true, data: results, message: 'New inserting successfully.' });
  });

});

//Update User
app.put(prefix + '/updatePollDetail_Table/:id', function ( req , res ) {
      let data = req.body;
      var id = req.params.id;
      delete data.id;

      if (!data) {
          return res.status(400).send({ error : true , message : 'Please provide body data' });
      }
      
      dbConfig.query ("update `PollDetail` set ? where `Que_ID` = ?", [data,id] 
      , function (error , results , fields) {
          if ( error ) console.log(error) ;
          return res.send({ success : true , data : results , message : 'Update xxx successfully' });
      })
});





//-------------------------------------------------------------//








//-------------------------------------------------------------//



/////////////////////////////////////////////////////////////////
///////////               Question                ///////////////
/////////////////////////////////////////////////////////////////


//Get all
app.get(prefix + '/getQuestion_Table',(req, res) => {
    let sql = "select * from `Question`"
    let query = dbConfig.query(sql, (err,results) => {
        if (err) { console.log(err) }
        console.log(results)

        return res.send(results);
    })
})

//Get by ID
app.get(prefix + "/getQuestion_Table/:id",(req , res) => {
  let sql = "select * from `Question` where Que_ID = " + req.params.id
  let query = dbConfig.query(sql, (err,results) => {
      if (err) { console.log(err) }
      
      console.log(results[0])

      return res.send(results[0]);
  });
  
});

///////////Get Emoji Marker
app.get(prefix + "/getQuestJoinReact_Table",(req , res) => {
    let sql = "SELECT l.Loc_ID, l.Loc_lat, l.Loc_long, r.React_feeling, r.Location_Loc_ID FROM Location l ,Reaction r WHERE l.Loc_ID = r.Location_Loc_ID ORDER BY l.Loc_ID "
    let query = dbConfig.query(sql, (err,results) => {
        if (err) { console.log(err) }
        
        console.log(results)
  
        return res.send(results);
    });
    
  });

//Create User
app.post(prefix + '/postQuestion_Table/', function(req,res) {
        
  //req.body = {id : 6 , email : 'email006', name : 'name006' , active : false };
  let data = req.body;
  //data.Reaction_React_ID = req.params.id
  //data.Que_ID = res.insertId
  let en_data = encode.encrypt(data);
  if (!encode.decrypt()) {
      return res.status(400).send({ success : false, message : 'Please Provide body' })
  }

  let json_encrypted = encode.decrypt()
  json_encrypted.Que_ID = parseInt(json_encrypted.Que_ID)
  

  dbConfig.query("insert into `Question` set ?", json_encrypted , function (error,results,fields) {
      console.log(error) ;
      console.log("save appt ---->");
      console.log(results)
      
      return res.send({ success: true, data: results, message: 'New inserting successfully.' });
  });

});

//Update User
app.put(prefix + '/updateQuestion_Table/:id', function ( req , res ) {
      let data = req.body;
      var id = req.params.id;
      delete data.id;

      if (!data) {
          return res.status(400).send({ error : true , message : 'Please provide body data' });
      }
      
      dbConfig.query ("update `Question` set ? where `Que_ID` = ?", [data,id] 
      , function (error , results , fields) {
          if ( error ) throw error ;
          return res.send({ success : true , data : results , message : 'Update xxx successfully' });
      })
});

//Delete User
app.delete(prefix + '/deleteQuestion_Table/:id', function ( req , res ) {
      let id = req.params.id;
      if(!id) {
          return res.status(400).send({ error : true , message : 'Please Provide xxx ID ' });
      }
      dbConfig.query('delete from `Question` where Que_ID = ?', [id], function (error , results , fields ) {
        if ( error ) console.log(error);
          return res.send({ success : true, data : results, message : 'xxx has been delete successfully.' });
      });
      console.log(typeof(id))
});

//NODE-CRON delete every 3 minutes

cron.schedule('* * * * * *',function(){
    console.log("CRON SQL COMMAND 1 IS ACTIVATED")
})

cron.schedule('*/3 * * * *',function(){
    dbConfig.query("DELETE FROM `Question` where Timestamp(Question.`Que_timestamp`) < (SELECT timestamp(now()))");
    console.log("CRON SQL COMMAND 2 IS ACTIVATED")
})

//-------------------------------------------------------------//



/////////////////////////////////////////////////////////////////
///////////               User                    ///////////////
/////////////////////////////////////////////////////////////////

//Get all
app.get(prefix + '/getUser_Table',(req, res) => {
    let sql = "select * from `User`"
    let query = dbConfig.query(sql, (err,results) => {
        if (err) { console.log(err) }
        console.log(results)

        return res.send(results);
    })
})

//Get by ID
app.get(prefix + "/getUser_Table/:phonenumber",(req , res) => {
  let sql = "select * from `User` where phonenumber = " + req.params.phonenumber
  let query = dbConfig.query(sql, (err,results) => {
      if (err) { console.log(err) }
      
      console.log(results[0])

      return res.send(results[0]);
  });
  
});

//Create User
app.post(prefix + '/postUser_Table/', function(req,res) {
        
  //req.body = {id : 6 , email : 'email006', name : 'name006' , active : false };
  let data = req.body;
  let en_data = encode.encrypt(data)
  if (!encode.decrypt()) {
      return res.status(400).send({ success : false, message : 'Please Provide body' })
  }
  let json_encrypted = encode.decrypt()
  json_encrypted.ID = parseInt(json_encrypted.ID)

  dbConfig.query("insert into `User` set ?", json_encrypted , function (error, results , fields) {
      console.log(error) ;
      console.log("save appt ---->");
      console.log(results)
      return res.send({ success: true, data: results, message: 'New inserting successfully.' });
  });

});

//Update User
app.put(prefix + '/updateUser_Table/:phonenumber', function ( req , res ) {
      let data = req.body;
      var phonenumber = req.params.phonenumber;
      delete data.phonenumber;

      if (!data) {
          return res.status(400).send({ error : true , message : 'Please provide body data' });
      }
      
      dbConfig.query ("update `User` set ? where `phonenumber` = ?", [data,phonenumber] 
      , function (error , results , fields) {
        if ( error ) console.log(error) ;
          return res.send({ success : true , data : results , message : 'Update xxx successfully' });
      })
});

//Delete User
app.delete(prefix + '/deleteUser_Table/:phonenumber', function ( req , res ) {
      let phonenumber = req.params.phonenumber;
      if(!phonenumber) {
          return res.status(400).send({ error : true , message : 'Please Provide xxx ID ' });
      }
      dbConfig.query('delete from `User` where phonenumber = ?', [phonenumber], function (error , results , fields ) {
        if ( error ) console.log(error);
          return res.send({ success : true, data : results, message : 'xxx has been delete successfully.' });
      });
      console.log(typeof(phonenumber))
});



//-------------------------------------------------------------//

/////////////////////////////////////////////////////////////////
///////////               Admin_mng                ///////////////
/////////////////////////////////////////////////////////////////
app.get(prefix + '/getAdminMng_Table',(req, res) => {
    let sql = "select * from `Admin_mng`"
    let query = dbConfig.query(sql, (err,results) => {
        if (err) { console.log(err) }
        console.log(results)
        
        return res.send(results);
    })
})

app.get(prefix + "/getAdminMng_Table/:id",(req , res) => {
  let sql = "select * from `Admin_mng` where am_id = " + req.params.id
  let query = dbConfig.query(sql, (err,results) => {
      if (err) { console.log(err) }
      
      console.log(results[0])
      
      return res.send(results[0]);
  });
  
});

//Create User
app.post(prefix + '/postAdminMng_Table/', function(req,res) {
        
  //req.body = {id : 6 , email : 'email006', name : 'name006' , active : false };
  let data = req.body;
  let en_data = encode.encrypt(data)
  if (!encode.decrypt()) {
      return res.status(400).send({ success : false, message : 'Please Provide body' })
  }
  let json_encrypted = encode.decrypt()

  /*
  Object.keys(json_encrypted).forEach(function(key) {
    json_encrypted[key] = parseInt(json_encrypted[key])
  })*/
  
  json_encrypted.am_id = parseInt(json_encrypted.am_id)

  dbConfig.query("insert into `Admin_mng` set ?", json_encrypted , function (error, results, fields) {
      console.log(json_encrypted)
      console.log(error) ;
      console.log("save appt ---->");
      console.log(results)
      return res.send({ success: true, data: results, message: 'New inserting successfully.' });
  });

});

//Update User
app.put(prefix + '/updateAdminMng_Table/:id', function ( req , res ) {
      let data = req.body;
      var id = req.params.id;
      delete data.id;

      if (!data) {
          return res.status(400).send({ error : true , message : 'Please provide body data' });
      }
      
      dbConfig.query ("update `Admin_mng` set ? where `am_id` = ?", [data,id] 
      , function (error , results , fields) {
        if ( error ) console.log(error) ;
          return res.send({ success : true , data : results , message : 'Update xxx successfully' });
      })
});

//Delete User
app.delete(prefix + '/deleteAdminMng_Table/:id', function ( req , res ) {
      let id = req.params.id;
      if(!id) {
          return res.status(400).send({ error : true , message : 'Please Provide xxx ID ' });
      }
      dbConfig.query('delete from `Admin_mng` where am_id = ?', [id], function (error , results , fields ) {
          if (error) throw error;
          return res.send({ success : true, data : results, message : 'xxx has been delete successfully.' });
      });
      console.log(typeof(id))
});
//-------------------------------------------------------------//






app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
})

/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////

app.listen(port, () => console.log(`Listening on port ${port}...`) );

/*
//Get By ID

    app.get(prefix + "/devinfo/:id",(req , res) => {
        let sql = "select * from `devinfo` where id = " + req.params.id
        let query = con.query(sql, (err,results) => {
            if (err) { console.log(err) }
            
            console.log(results[0])

            return res.send(results[0]);
        });
        
    });

*/

